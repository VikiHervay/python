# A module is basically a file containing a set of functions to include in your application. There are core python modules, modules you can install using the pip package manager (including Django) as well as custom modules

# core python modules
import datetime
from datetime import date
import time

# pip module
from camelcase import CamelCase # a camelcase module-ból importálja a CamelCase metódust

# import custom module
import validator
from validator import validate_email

# today = datetime.date.today() -> ez akkor hogyha csak simán a datetime-ot importáljuk
today = date.today()
print(today)
print('--')

timestamp = time.time()
print(timestamp)
print('--')

# camelcase core module ->pip3 install camelcase
c = CamelCase()
print(c.hump('hello there world')) #csupa nagybetűssé alakítja

# email
email = 'test@test.com'
if validate_email(email):
    print('Email is valid')
else:
    print('Email is not valid')