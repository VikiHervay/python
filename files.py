# Python has functions for creating, reading, updating, and deleting files.

# open/create a file
myFile = open('myfile.txt', 'w')

# get some info

print('Name: ', myFile.name)
print('is closed: ', myFile.closed)
print('Opening mode: ', myFile.mode)

# write to file

myFile.write('I love python')
myFile.write(' and js')
myFile.close()

# append to file
myFile = open('myfile.txt', 'a')
myFile.write('\nI also like php')
myFile.close()

# read from a file
myFile = open('myfile.txt', "r+")
text = myFile.read(100)
print(text)