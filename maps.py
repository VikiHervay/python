
from random import shuffle

def jumble(word):
    #átkasztolja listává -> ['b', 'e', 'e', 't','r', 'o', 'o', 't']
    anagram = list(word)
    #összekeveri a betűket
    shuffle(anagram)
    #a '-' lesz amit a betűk közé rak, ha empty string akkor ugye visszafűzi őket egy szóvá
    return ''.join(anagram)

words = ['beetroot', 'carrots', 'potatoes']
anagrams = []
#végigiterálhatunk rajta
for word in words:
    anagrams.append(jumble(word))

print(anagrams)
#vagy a rövidebb map technikát is használhatjuk
#a function ami vár egy darabot a datából...
#map(function, data)
print(list(map(jumble, words)))

#comprehension method
print([jumble(word) for word in words])