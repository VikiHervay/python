# If/ Else conditions are used to decide to do something based on something being true or false
x = 10
y = 20

# Comparison Operators (==, !=, >, <, >=, <=) - Used to compare values
    # if
if x > y :
    print(f'{x} is greater than {y}')

    # else
if x > y :
    print(f'{x} is greater than {y}')
else:
    print(f'{y} is greater than {x}') 
    
    # elseif
if x > y :
    print(f'{x} is greater than {y}')
elif x == y:
     print(f'{x} is equal {y}')
else:
    print(f'{y} is greater than {x}') 

    # nested if
if x > 2:
    if x <= 10:
        print(f'{x} is greater than 2 and less than or equal to 10')


# Logical operators (and, or, not) - Used to combine conditional statements

# AND
if x > 2 and x <= 10:
    print(f'{x} is greater than 2 and less than or equal to 10')
# OR
if x > 2 or x <= 10:
    print(f'{x} is greater than 2 and less than or equal to 10')
# NOT
if not(x == y):
    print(f'{x} is not equal to {y}')


# Membership Operators (in, not in) - Membership operators are used to test if a sequence is presented in an object
# in
z = 3
numbers = [1, 2,3,4,5]
if z in numbers:
    print(z in numbers) #true

    # not in 
u = 13
numbers2 = [1,2,3,4,5]
if u not in numbers2:
    print(u not in numbers2) #true, mert 13 nincs benne a numbers2 arrayben

# Identity Operators (is, is not) - Compare the objects, not if they are equal, but if they are actually the same object, with the same memory location:
h = 1
k = 1
if h is k:
    print(h is k) #true mert ugyanaz az értéke

# is not

l= 1
e =2
if l is not e:
    print(l is not e)