#double prize money weekend bonaza
prizes = [5,10,50,100,1000]

dbls_prizes = []
for prize in prizes:
    dbls_prizes.append(prize*2)

print(dbls_prizes)

#comprehension method ->rövidebb
double_prizes = [prize*2 for prize in prizes]
print(double_prizes)

#squaring numbers
nums= [1,2,3,4,5,6,7,8,9,10]

squared_even_nums = []

for item in nums:
    if(item **2) % 2 == 0:
        squared_even_nums.append(item **2)
print(squared_even_nums)

#comprehension method

squared = [num**2 for num in nums if(num**2) % 2 == 0]
print(squared)