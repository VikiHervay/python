# ipsum_file = open('Files/ipsum.txt')

# # for line in ipsum_file:
# #     #line.rstrip() <-- a line-ok közötti entereket levágja
# #     print(line)

# # #list lesz belőle
# # ipsum_file.seek(0) #-->visszamegy a file elejére
# # lines = ipsum_file.readlines()
# # print(lines)

# ipsum_file.seek(50) #50. karakter a fileban
# file_text = ipsum_file.read(100) #attól ponttól 100 karaktert vesz
# print(file_text)

# ipsum_file.close()
#eldönti hogy a kacsacsőr benne van-e
def sequence_filter(line):
    return '>' not in line
    
#ha ezt a with-et használjuk nem kell lezárni, ő gondoskodik róla
with open('Files/dna.txt') as file:
    lines = file.readlines()
    #csak azokat teszi bele ahol nincs kacsacsőr az elején
    print(list(filter(sequence_filter, lines)))

