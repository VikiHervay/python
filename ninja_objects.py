class Planets:
    #class level attributes (fields)
    #nem unique mert minden planétára igaz
    shape = 'round'

    def __init__(self, name, radius, grav, system):
        #instance attributes
        self.name = name
        self.radius = radius
        self.gravity = grav
        self.system =  system

    #instance methods
    def AddName(self, input):
        self.name = input

newplanet = Planets('Hoth', 20000, 5.5, 'Hoth')
print(f'Name is {newplanet.name}')
print(f'Radius is {newplanet.radius}')

newplanet.AddName('Batuu')
print(f'New name is {newplanet.name}')

naboo = Planets('Naboo', 300000, 8, 'Naboo System')
print(f'Name is {naboo.name}')

print(Planets.shape)


