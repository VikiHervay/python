# A Tuple is a collection which is ordered and unchangeable. Allows duplicate members.

#  create tuple
fruits = ('apples', 'oranges', 'grapes')
# constructor
fruits1 = tuple(('apple', 'orange', 'grape'))

# fruits2 = ('Apples') type(fruits) --> stringként kezeli
fruits2 = ('Apples',) #így már 'array'ként kezeli a vessző miatt
print(fruits, type(fruits2))
# get a value
print(fruits[1])

# unchangeable !!
# fruits[0] = 'Pear'
# print(fruits)

# delete
del fruits1 #kitörli a definiált változót

# lenght
print(len(fruits))


# A Set is a collection which is unordered and unindexed. No duplicate members.
# create set
print( {1,2,3} | {3,4,5} ) #az uniója
print( {1,2,3} - {3,4,5} ) #ami az elsőben van
print( {1,2,3} & {3,4,5} ) #metcet
print( {1,2,3} ^ {3,4,5} ) #kihagyja azt ami mindkettőben benne van
fruits_set={'Apples', 'Oranges', 'Mango'}
print('Apples' in fruits_set)

# add to set
fruits_set.add('Grape')
print(fruits_set)

# remove from set
fruits_set.remove('Grape')
print(fruits_set)

# add duplicate
fruits_set.add('Apples')
print(fruits_set) #nem add errort, csak simán nem adja hozzá még egyszer az almát
# # clear set - létezik csak üres
# fruits_set.clear()
# print(fruits_set)

