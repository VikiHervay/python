# Strings in python are surrounded by either single or double quotation marks. Let's look at string formatting and some string methods
name = 'Cassie'
age = 30

#concatenate -- int-et nem lehet összefűzni, ezért kell az str
print('Hello my name is ' + name + ' and I am ' + str(age))

# String Formatting
#positional arguments
print('My name is {name} and I am {age}'.format(name = name,age = age))

#f-string(3.6+)
print(f'Hello my name is {name} and I am {age}')

# String Methods
s = 'hellow-orld'
#Capitalize
print(s.capitalize())
#mare all uppercase
print(s.upper())
# make all lower
print(s.lower())
# swap case
print(s.swapcase())
# get length
print(len(s))
# replace
print(s.replace('world', 'everyone'))
# count -- az s nevű változóban számolja a h betűket
sub = 'h'
print(s.count(sub))
# starts with
print(s.startswith('hello'))
# ends with
print(s.endswith('d'))
# split into a list
print(s.split('-'))
#find position
print(s.find('e')+1)
# is all alphanumeric -false (a space meg a dash miatt)
print(s.isalnum())
# is all alphabetic -false (a space meg a dash miatt)
print(s.isalpha())
# is all numeric -false
print(s.isnumeric())

