def cough_dec(func):

    def func_wrapper():
        #code before function
        print('*cough*')
        func()
        # code after function
        print('*cough*')
    
    return func_wrapper


@cough_dec
def question():
    print('can you give me a discount?')

@cough_dec
def answer():
    #a double quotemark miatt kiírja az s előtti kis izét aminek nem tudom a hivatalos nevét
    print("it's only 50p your cheapskate")

question()
answer()