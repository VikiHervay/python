v = list(set("szeptember"))
v.sort()
print(v)

# A List is a collection which is ordered and changeable. Allows duplicate members.
# mint az array PHPban pl
# create list
numbers = [1,2,3,4,5]
fruits = ['apples', 'oranges', 'grapes', 'pears']
# use constructor
numbers2 = list((1,2,3,4,5,6))
print(numbers, numbers2) 
# get a single value from the list
print(fruits[1])
# get length of the list
print(len(fruits))
# add the end of the list (append)
fruits.append('mangos')
print(fruits)
# remove
print(fruits)
# insert into a certain position
fruits.insert(2, 'strawberries')
print(fruits)
# remove from certain position (pop)
fruits.pop(2)
print(fruits)
# reverse a list
fruits.reverse()
print(fruits)
# sort list
fruits.sort()
print(fruits)
# reverse sort
fruits.sort(reverse=True)
print(fruits)
# change a value
fruits[0] = 'Blueberries'
a = [1,2,3,4,5]
#a -1 az a végétől megy vissza 1-et
#a : azt adja meg, hogy a 2. elemtől jobbra, ha előtte van akkor balra
print(a[2:] , a[:2] , a[-1])
#A két ** négyzetre emeli
print( [ x**2 for x in [1,2,3] ] )
# 0,1,2,3 -> ezt adja vissza
print( list(range(4)) )
# 1,2,3 -> ezt adja vissza
print( list(range(1,4)) )

print( [0] * 3 ) #--> [0,0,0]
print(fruits)



