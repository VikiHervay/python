with open('Files/write.txt', 'w') as write_file:
    write_file.write("Hey there")
    
# #újra fogja írni, az első sor eltűnik
# with open('Files/write.txt', 'w') as write:
#     write.write('\nHey there2')

#az 'a' betű csak hozzáfűzi
with open('Files/write.txt', 'a') as write:
    write.write('\nHey there2')

quotes = [
    '\nI can resist everything except temptation',
    '\nWe are all in the gutter, but some of us are looking at the stars',
    '\nAlways forgive your enemies - nothing annoys them so much'
]

with open('Files/write.txt', 'a') as write:
    #writelines listát vár argumentumként
    write.writelines(quotes)