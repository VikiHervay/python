#hasonló mint a map, vár egy functiont meg egy collectiont és a function filtereli a collection-t valami alapján
grades = ['A', 'B', 'C', 'D', 'F', 'A', 'F']
#szóval akkor tér vissza igazzal, ha a grade nem F, ha azt írom hogy 'F' in grade akkor az F-eket őrzi meg
def testing_function(grade):
    return 'F' not in grade

#ez a filter dolog azokat az elemeket tartja meg a filterelt listában amikre a function igazzal tért vissza
print(list(filter(testing_function, grades)))

good_Grades = [grade for grade in grades if grade != 'F']
print(good_Grades)