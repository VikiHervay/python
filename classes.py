# A class is like a blueprint for creating objects. An object has properties and methods(functions) associated with it. Almost everything in Python is an object

# create class
class User:
    # constructor
    def __init__(self, name, email, age):
        self.name = name
        self.email = email
        self.age = age
    
    def greeting(self):
        return f'My name is {self.name} and I am {self.age}'
    def has_birthday(self):
        self.age += 1



# extend class
class Customer(User):

    # constructor
    def __init__(self, name, email, age):
        self.name = name
        self.email = email
        self.age = age
        self.balance = 0

    def set_balance(self, bal):
        self.balance = bal

    def greeting(self):
        return f'My name is {self.name} and I am {self.age} and my balance is {self.balance}'


    # init user object
brad = User('Brad', 'email', 30)
janet = Customer('Jannet', 'email', 25)

janet.set_balance(500)

print(janet.greeting())

brad.has_birthday()
print(brad.greeting())