# A Dictionary is a collection which is unordered, changeable and indexed. No duplicate members.
# create Dictionary
person = {
    'first_name': 'Cassie',
    'last_name': 'Railly',
    'age': 30
}
# constructor
person2 = dict(first_name="Cole", last_name="James")
print(person, type(person))
print(person2, type(person2))

# get value
print(person['first_name'])
print(person.get('last_name'))

# add key/value
person['phone'] = '555-555'
print(person)

# get dict keys
print(person.keys())

# get dict items
print(person.items())

# copy a Dictionary
person2 = person.copy()
person2['city'] = 'Boston'
print(person2)

# remove an item
del(person['age'])
person.pop('phone')
print(person)

# length how many key-value pairs are in
print(len(person2))

# list of dict
people = [
    {'name': 'Martha', 'age': 30},
    {'name': 'Kevin', 'age': 25}
]
people.append(person2)
print(people[0]['name'])
# # clear
# person.clear()
# print(person)

a={'a': 1 , 'b': 2} 
a.update({'d' : 4})
print( *a ) #--> tuple lesz belőle?!
c={**a,**{'e':5}} # csillag-csillag operátor
print( dict([('a',1) , ('b',2) , ('c',3) ]) ) # tuple, amit tömbbe teszünk és végül dict -> kb mint egy json lesz a vége