# A variable is a container for a value, which can be of various types

'''
This is a 
multiline comment
or docstring (used to define a functions purpose)
can be single or double quotes
'''

"""
VARIABLE RULES:
  - Variable names are case sensitive (name and NAME are different variables)
  - Must start with a letter or an underscore
  - Can have numbers but can not start with one
"""
#nem kell pontosvessző, azt se kell megmondani hogy ez most int vagy string
x = 1 # int
y = 2.5 #float
name = 'John' #string single or double quotes both works
is_cool = True #bool capital first letter is a must in this case

#Multipla assignment
x1, y1, name1, is_cool1 = (1, 2.5, 'John', True)
# basic math
a = x + y1
#casting type(variable_name)<-- visszaadja hogy miféle
x = str(x)
print(f'type of x at this pont {type(x)}')
y = int(y)
z = float(y) #visszakasztolásnál elveszik a 2.5 five része, 2.0 lesz

print(type(y), y)
